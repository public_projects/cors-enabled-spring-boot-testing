/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.orange;

//import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * http://localhost:8080/test1
 * @author greenhorn
 */
@RestController
public class TestController {

    @CrossOrigin
    @RequestMapping(value = "/test1", method = RequestMethod.POST)
    public String test1() {
        System.err.println("[test1]");
        return "Test cors";
    }
}
